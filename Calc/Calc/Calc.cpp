// Calc.cpp : Definiuje eksportowane funkcje dla aplikacji DLL.
//

#include "stdafx.h"

#include <iostream>

extern "C" {

	__declspec(dllexport) float * add_vectors(float * a, float * b, int size) {
		float * c = new float[size];

		for (int i = 0; i < size; ++i) {
			c[i] = a[i] + b[i];
		}

		return c;
	}
	__declspec(dllexport) float * subtract_vectors(float * a, float * b, int size) {
		float * c = new float[size];

		for (int i = 0; i < size; ++i) {
			c[i] = a[i] - b[i];
		}

		return c;
	}

	__declspec(dllexport) float calculate_scalar_product(float * a, float * b, int size) {
		float val = 0;

		if (a == nullptr) return NULL;
		if (b == nullptr) return NULL;

		for (int i = 0; i < size; ++i) {
			val += a[i] * b[i];
		}

		return val;
	}
}