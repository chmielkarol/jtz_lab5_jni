package lab05;

public class Calc {
	
	public Double[] a;
	public Double[] b;
	public Double c;
	
	public int dim;
	
    static {
        System.load("D:\\Users\\Karol\\Documents\\Uczelnia\\Semestr VI\\JAVA\\lab\\lab05\\bin\\lab05\\Calc.dll");
    } 

	public native Double multi01(Double[] a, Double[] b);
	// zak�adamy, �e po stronie kodu natywnego wyliczony zostanie iloczyn skalarny dw�ch wektor�w
	
	public native Double multi02(Double[] a); 
	// zak�adamy, �e drugi atrybut b�dzie pobrany z obiektu przekazanego do metody natywnej 
	
	public native void multi03();
	// zak�adamy, �e po stronie natywnej utworzone zostanie okienko na atrybuty,
	// a po ich wczytaniu i przepisaniu do a,b obliczony zostanie wynik. 
	// Wynik powinna wylicza� metoda Javy multi04 
	// (korzystaj�ca z parametr�w a,b i wpisuj�ca wynik do c).

	public void multi04(){
	 // mno�y a i b, wynik wpisuje do c
		if(a.length == b.length) {
			dim = a.length;
			c = 0.0;
			for(int i = 0; i < dim; ++i) {
				c += (a[i] * b[i]);
			}
		}
	 }
	
}
