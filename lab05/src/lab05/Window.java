package lab05;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.JToggleButton;
import javax.swing.JSpinner;
import javax.swing.JComboBox;

public class Window {

	private JFrame frmIloczynSkalarny;
	
	private static Calc calc = new Calc();
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_6;
	
	private JButton btnPodajWsprzdne;
	private JButton btnPodajWsprzdne_3;
	private JButton btnOblicz;
	private JComboBox comboBox;
	private JButton btnWykonaj;
	private JTextField textField_3;
	private JPanel panel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmIloczynSkalarny.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIloczynSkalarny = new JFrame();
		frmIloczynSkalarny.setResizable(false);
		frmIloczynSkalarny.setIconImage(Toolkit.getDefaultToolkit().getImage(Window.class.getResource("/lab05/icon.png")));
		frmIloczynSkalarny.setTitle("Iloczyn skalarny");
		frmIloczynSkalarny.setBounds(100, 100, 450, 381);
		frmIloczynSkalarny.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmIloczynSkalarny.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("multi01 / multi02", null, panel, null);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {55, 263, 55};
		gbl_panel.rowHeights = new int[]{14, 56, 56, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Liczba wymiar\u00F3w przestrzeni", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_9 = new GridBagConstraints();
		gbc_panel_9.insets = new Insets(0, 0, 5, 0);
		gbc_panel_9.fill = GridBagConstraints.BOTH;
		gbc_panel_9.gridx = 1;
		gbc_panel_9.gridy = 0;
		panel.add(panel_9, gbc_panel_9);
		
		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setColumns(2);
		panel_9.add(textField_6);
		
		JButton btnPodaj = new JButton("Podaj\r\n");
		btnPodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String input = JOptionPane.showInputDialog(null, "Podaj liczb� wymiar�w przestrzeni: ");
				if(input != null) {
					try {
						int dim = Integer.valueOf(input);
						textField_6.setText(input);
						calc.dim = dim;
						btnPodajWsprzdne.setEnabled(true);
					}
					catch(NumberFormatException e) {
						
					}
				}
			}
		});
		panel_9.add(btnPodaj);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Pierwszy wektor", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_3.anchor = GridBagConstraints.NORTH;
		gbc_panel_3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_3.gridx = 1;
		gbc_panel_3.gridy = 1;
		panel.add(panel_3, gbc_panel_3);
		
		textField = new JTextField();
		textField.setEditable(false);
		panel_3.add(textField);
		textField.setColumns(10);
		
		btnPodajWsprzdne = new JButton("Podaj wsp\u00F3\u0142rz\u0119dne");
		btnPodajWsprzdne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc.a = new Double[calc.dim];
				String text = "[";
				for(int i = 0; i < calc.dim; ++i) {
					Double val = 0.0;
					String input = "";
					while(true) {
						try{
							input = JOptionPane.showInputDialog(null, "Podaj " + String.valueOf(i+1) + ". wsp�rz�dn�: ");
							val = Double.parseDouble(input);
							break;
						}
						catch(NumberFormatException e1) {
						}
					}
					calc.a[i] = val;
					text += input + ',';
				}
				text = text.substring(0, text.length() - 1) + ']';
				textField.setText(text);
				btnPodajWsprzdne_3.setEnabled(true);
			}
		});
		btnPodajWsprzdne.setEnabled(false);
		panel_3.add(btnPodajWsprzdne);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Drugi wektor", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_4.insets = new Insets(0, 0, 5, 0);
		gbc_panel_4.anchor = GridBagConstraints.NORTH;
		gbc_panel_4.gridx = 1;
		gbc_panel_4.gridy = 2;
		panel.add(panel_4, gbc_panel_4);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setColumns(10);
		panel_4.add(textField_1);
		
		btnPodajWsprzdne_3 = new JButton("Podaj wsp\u00F3\u0142rz\u0119dne");
		btnPodajWsprzdne_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc.b = new Double[calc.dim];
				String text = "[";
				for(int i = 0; i < calc.dim; ++i) {
					Double val = 0.0;
					String input = "";
					while(true) {
						try{
							input = JOptionPane.showInputDialog(null, "Podaj " + String.valueOf(i+1) + ". wsp�rz�dn�: ");
							val = Double.parseDouble(input);
							break;
						}
						catch(NumberFormatException e1) {
						}
					}
					calc.b[i] = val;
					text += input + ',';
				}
				text = text.substring(0, text.length() - 1) + ']';
				textField_1.setText(text);
				btnOblicz.setEnabled(true);
			}
		});
		btnPodajWsprzdne_3.setEnabled(false);
		panel_4.add(btnPodajWsprzdne_3);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "WYNIK", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.insets = new Insets(0, 0, 5, 0);
		gbc_panel_5.fill = GridBagConstraints.BOTH;
		gbc_panel_5.gridx = 1;
		gbc_panel_5.gridy = 4;
		panel.add(panel_5, gbc_panel_5);
		
		btnOblicz = new JButton("Oblicz");
		btnOblicz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = comboBox.getSelectedIndex();
				Double result = 0.0;
				switch(index) {
				case 0:
					result = calc.multi01(calc.a, calc.b);
					break;
				case 1:
					result = calc.multi02(calc.a);
				}
				textField_2.setText(Double.toString(result));
			}
		});
		String[] model = {"multi01", "multi02"};
		comboBox = new JComboBox(model);
		panel_5.add(comboBox);
		btnOblicz.setEnabled(false);
		panel_5.add(btnOblicz);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setColumns(5);
		panel_5.add(textField_2);
		
		JPanel panel_8 = new JPanel();
		tabbedPane.addTab("multi03", null, panel_8, null);
		GridBagLayout gbl_panel_8 = new GridBagLayout();
		gbl_panel_8.columnWidths = new int[] {55, 263, 55};
		gbl_panel_8.rowHeights = new int[]{56, 56, 0};
		gbl_panel_8.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_8.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_8.setLayout(gbl_panel_8);
		
		btnWykonaj = new JButton("Wykonaj");
		btnWykonaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc.multi03();
				textField_3.setText(Double.toString(calc.c));
			}
		});
		GridBagConstraints gbc_btnWykonaj = new GridBagConstraints();
		gbc_btnWykonaj.insets = new Insets(0, 0, 5, 5);
		gbc_btnWykonaj.gridx = 1;
		gbc_btnWykonaj.gridy = 0;
		panel_8.add(btnWykonaj, gbc_btnWykonaj);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Wynik", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 0, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 1;
		panel_8.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{263, 0};
		gbl_panel_1.rowHeights = new int[]{56, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		textField_3 = new JTextField();
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 0;
		gbc_textField_3.gridy = 0;
		panel_1.add(textField_3, gbc_textField_3);
		textField_3.setColumns(5);
	}

}
